package com.zhangu.nutrition_calculator.AppButtons;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by harryfeng on 2017-04-26.
 */

public class FoodCategoryButton extends Button {
    public FoodCategoryButton(Context context) {
        super(context);

        init(null);
    }

    public FoodCategoryButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FoodCategoryButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public FoodCategoryButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(attrs);
    }

    private void init(@Nullable AttributeSet set){
        //this.setBackgroundColor(Color.BLUE);
    }
}
