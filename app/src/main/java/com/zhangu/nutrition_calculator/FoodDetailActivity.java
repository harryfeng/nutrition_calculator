package com.zhangu.nutrition_calculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by harryfeng on 2017-04-28.
 */

public class FoodDetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);
        Intent intent = getIntent();
        AVObject food = (AVObject) intent.getExtras().get("food");

        ((TextView)findViewById(R.id.foodName)).setText(food.getString("name"));
        ((TextView)findViewById(R.id.reLiang)).setText((Double.toString(food.getDouble("energy"))+"千卡"));
        ((TextView)findViewById(R.id.protein)).setText(Double.toString(food.getDouble("protein"))+"克");
        ((TextView)findViewById(R.id.fat)).setText(Double.toString(food.getDouble("fat"))+"克");
        ((TextView)findViewById(R.id.carbonHydrate)).setText(Double.toString(food.getDouble("carbonHydrate"))+"克");
        ((TextView)findViewById(R.id.shanShiQianWei)).setText(Double.toString(food.getDouble("fiber"))+"克");
        ImageLoader.getInstance().displayImage(food.getAVFile("foodImage").getUrl(), (ImageView)findViewById(R.id.foodImage));
    }
}
