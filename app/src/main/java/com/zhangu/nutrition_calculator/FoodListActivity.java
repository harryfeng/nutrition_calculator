package com.zhangu.nutrition_calculator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.zhangu.nutrition_calculator.AppAdapter.FoodListAdapter;

import java.util.List;

/**
 * Created by harryfeng on 2017-04-27.
 */

public class FoodListActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);

        Intent intent = getIntent();
        AVObject categoryAvObj = (AVObject) intent.getExtras().get("categoryAvObj");

        final ListView foodListView = (ListView)findViewById(R.id.foodList);

        AVQuery<AVObject> query = new AVQuery<>("Food");
        query.whereEqualTo("category", categoryAvObj);
        // 如果这样写，第二个条件将覆盖第一个条件，查询只会返回 priority = 1 的结果

        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if(e == null){
                    ListAdapter foodListAdapter = new FoodListAdapter(FoodListActivity.this,list);

                    final List<AVObject> _list = list;
                    foodListView.setAdapter(foodListAdapter);
                    foodListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Log.d("click","food");
                            Intent intent = new Intent(FoodListActivity.this, FoodDetailActivity.class);
                            AVObject food = (AVObject)_list.get(i);
                            intent.putExtra("food",food);
                            startActivity(intent);
                        }
                    });

                }else{
                    Context context = getApplicationContext();
                    CharSequence text = e.getMessage();
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        });
    }
}
