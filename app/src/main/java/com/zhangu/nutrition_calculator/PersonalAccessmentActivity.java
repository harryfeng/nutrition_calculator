package com.zhangu.nutrition_calculator;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by harryfeng on 2017-04-28.
 */

public class PersonalAccessmentActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_accessment);
        ((Button)findViewById(R.id.submitButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    DatePicker dataPicker = (DatePicker)findViewById(R.id.birthdayDatePicker);
                    Date birthday = getDateFromDatePicker(dataPicker);
                    String weightStr = ((EditText)findViewById(R.id.weightEditText)).getText().toString();
                    Double weight = Double.valueOf(weightStr);
                    Double height = Double.valueOf(((EditText)findViewById(R.id.heightEditText)).getText().toString());
                    String gener = ((RadioButton)findViewById(((RadioGroup)findViewById(R.id.genderRadioGroup)).getCheckedRadioButtonId())).getText().toString();
                    String activity = ((RadioButton)findViewById(((RadioGroup)findViewById(R.id.activityRadioGroup)).getCheckedRadioButtonId())).getText().toString();

                    AVUser user = AVUser.getCurrentUser();
                    user.put("birthday",birthday);
                    user.put("weight",weight);
                    user.put("height",height);
                    user.put("gender",gener);
                    user.put("activity",activity);
                    user.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            if(e == null){
                                Toast.makeText(getApplicationContext(), "成功生成评测报告", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }catch (Exception e){
                    Context context = getApplicationContext();
                    CharSequence text = e.getMessage();
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, "请完善信息", duration);
                    toast.show();
                }

            }
        });
    }

    public static java.util.Date getDateFromDatePicker(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }
}
