package com.zhangu.nutrition_calculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.avos.avoscloud.AVUser;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
/**
 * Created by harryfeng on 2017-04-29.
 */

public class HealthReportActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_report);
        AVUser user = AVUser.getCurrentUser();
        Double weight = user.getDouble("weight");
        Double height = user.getDouble("height");
        String gender = user.getString("gender");
        Date birthday = user.getDate("birthday");
        String activity = user.getString("activity");

        int age = getDiffYears(new Date(),birthday);
        Double bmi = weight/(height * height);
        int genderInteger = (gender == "男")?1:0;

        Double tiZhiLv = bmi*1.2 + 0.23*age - 5.4 -10.8*genderInteger;
        String bmiCategory = "";
        if(bmi < 18.5){
            bmiCategory = "偏瘦";
        }else if(bmi>18.5 && bmi<23.9){
            bmiCategory = "标准";
        }else if(bmi>24 && bmi<27.9){
            bmiCategory = "偏胖";
        }else{
            bmiCategory = "过胖";
        }
        Double sheRuRangeA;
        Double sheRuRangeB;
        switch (activity){
            case "轻度":
                sheRuRangeA = 26*weight;
                sheRuRangeB = 30*weight;
                break;
            case "中度":
                sheRuRangeA = 31*weight;
                sheRuRangeB = 37*weight;
                break;
            case "重度":
                sheRuRangeA = 38*weight;
                sheRuRangeB = 40*weight;
                break;
            default:
                sheRuRangeA = 0.0;
                sheRuRangeB = 0.0;
        }

        String sheRuRange = sheRuRangeA.toString() + "kcal" + " ~ " + sheRuRangeB.toString() + "kcal";


        ((TextView)findViewById(R.id.BMI)).setText(bmi.toString());
        ((TextView)findViewById(R.id.BMICategory)).setText(bmiCategory);
        ((TextView)findViewById(R.id.TiZhi)).setText(tiZhiLv.toString());
        ((TextView)findViewById(R.id.calculatedReLiangIntake)).setText(sheRuRange);

        ((Button)findViewById(R.id.reaccessButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HealthReportActivity.this, PersonalAccessmentActivity.class);
                startActivity(intent);
            }
        });

    }

    public static int getDiffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }
}
