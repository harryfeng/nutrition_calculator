package com.zhangu.nutrition_calculator.AppAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.zhangu.nutrition_calculator.R;

import java.util.List;

import static com.zhangu.nutrition_calculator.R.id.foodCategoryName;

/**
 * Created by harryfeng on 2017-04-27.
 */

public class FoodCategoryAdapter extends ArrayAdapter{

    public FoodCategoryAdapter(Context context, List<AVObject> list) {
        super(context, R.layout.row_food_category,list);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater foodListLayoutInflator = LayoutInflater.from(getContext());

        View view = foodListLayoutInflator.inflate(R.layout.row_food_category,parent,false);

        AVObject foodCategoryAvObj = (AVObject) getItem(position);

        TextView text = (TextView)view.findViewById(foodCategoryName);

        text.setText(foodCategoryAvObj.getString("name"));

        return view;
    }
}
