package com.zhangu.nutrition_calculator.AppAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zhangu.nutrition_calculator.R;

import java.util.List;

/**
 * Created by harryfeng on 2017-04-29.
 */

public class DietRecordAdapter extends ArrayAdapter {
    public DietRecordAdapter(Context context, List<AVObject> list) {
        super(context, R.layout.row_food_record,list);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater foodListLayoutInflator = LayoutInflater.from(getContext());

        View view = foodListLayoutInflator.inflate(R.layout.row_food_record,parent,false);

        AVObject foodRecord = (AVObject) getItem(position);
        AVObject food = (AVObject)foodRecord.get("food");
        ((TextView)view.findViewById(R.id.foodName)).setText(food.getString("name") + " -- " + foodRecord.getString("mealType"));
        Double quantity = foodRecord.getDouble("quantity");
        ((TextView)view.findViewById(R.id.foodQuantity)).setText(Double.toString(quantity) + "克");
        Double totalEnergy = quantity * food.getDouble("energy")/100;
        ((TextView)view.findViewById(R.id.totalEnergy)).setText(Double.toString(totalEnergy) + "千卡");

        ImageLoader.getInstance().displayImage(food.getAVFile("foodImage").getUrl(), (ImageView)view.findViewById(R.id.foodImage));

        return view;
    }
}
