package com.zhangu.nutrition_calculator.AppAdapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zhangu.nutrition_calculator.R;

import java.util.List;

/**
 * Created by harryfeng on 2017-04-27.
 */

public class FoodListAdapter extends ArrayAdapter {
    public FoodListAdapter(Context context, List<AVObject> list) {
        super(context, R.layout.row_food,list);
    }
    private Bitmap mIcon_val;
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater foodListLayoutInflator = LayoutInflater.from(getContext());

        View view = foodListLayoutInflator.inflate(R.layout.row_food,parent,false);

        AVObject food = (AVObject) getItem(position);

        ((TextView)view.findViewById(R.id.foodName)).setText(food.getString("name"));
        ((TextView)view.findViewById(R.id.foodCalori)).setText(Double.toString(food.getDouble("energy")) + "千卡/100克");
        ImageLoader.getInstance().displayImage(food.getAVFile("foodImage").getUrl(), (ImageView)view.findViewById(R.id.foodImageView));

        return view;
    }
}
