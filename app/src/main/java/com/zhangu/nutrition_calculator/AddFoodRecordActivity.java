package com.zhangu.nutrition_calculator;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;
import com.zhangu.nutrition_calculator.AppAdapter.FoodListAdapter;

import java.util.List;

/**
 * Created by harryfeng on 2017-04-29.
 */

public class AddFoodRecordActivity extends AppCompatActivity {
    private Integer quantityResult;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food_record);
        Intent intent = getIntent();
        final String mealType = (String) intent.getExtras().get("mealType");

        final ListView foodListView = (ListView)findViewById(R.id.foodList);
        AVQuery<AVObject> query = new AVQuery<>("Food");
        //query.whereEqualTo("category", categoryAvObj);
        // 如果这样写，第二个条件将覆盖第一个条件，查询只会返回 priority = 1 的结果

        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if(e == null){
                    ListAdapter foodListAdapter = new FoodListAdapter(AddFoodRecordActivity.this,list);

                    final List<AVObject> _list = list;
                    foodListView.setAdapter(foodListAdapter);
                    foodListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            final AVObject currentFood = _list.get(i);
                            Log.d("click","food");
                            Integer quantity = 0;
                            LayoutInflater li = LayoutInflater.from(getApplicationContext());
                            View promptsView = li.inflate(R.layout.dialog_food_record, null);
                            final TextView resultQuantityTextView = (TextView)promptsView.findViewById(R.id.quantityResult);
                            ((SeekBar)promptsView.findViewById(R.id.quantitySlider)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                @Override
                                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                    setQuantityResult(i);
                                    resultQuantityTextView.setText(Integer.toString(i) + "克");
                                }

                                @Override
                                public void onStartTrackingTouch(SeekBar seekBar) {

                                }

                                @Override
                                public void onStopTrackingTouch(SeekBar seekBar) {

                                }
                            });

                            AlertDialog alertDialog = new AlertDialog.Builder(AddFoodRecordActivity.this).create();
                            alertDialog.setTitle(mealType);
                            alertDialog.setMessage("Alert message to be shown");
                            alertDialog.setView(promptsView);
                            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                           // dialog.dismiss();
                                            Log.d("click","k button");
                                            AVObject todo = new AVObject("DietRecord");
                                            todo.put("quantity", AddFoodRecordActivity.this.quantityResult);
                                            todo.put("user", AVUser.getCurrentUser());
                                            todo.put("food", currentFood);
                                            todo.put("mealType", mealType);
                                            todo.saveInBackground(new SaveCallback() {
                                                @Override
                                                public void done(AVException e) {
                                                    if (e == null) {
                                                        finish();
                                                    } else {
                                                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });

                                        }
                                    });
                            alertDialog.show();
//                            Intent intent = new Intent(AddFoodRecordActivity.this, FoodDetailActivity.class);
//                            AVObject food = (AVObject)_list.get(i);
//                            intent.putExtra("food",food);
//                            startActivity(intent);
                        }
                    });

                }else{
                    Context context = getApplicationContext();
                    CharSequence text = e.getMessage();
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        });
    }

    private void setQuantityResult(int i){
        this.quantityResult = i;
    }
}
