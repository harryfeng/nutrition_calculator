package layout;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.zhangu.nutrition_calculator.AddFoodRecordActivity;
import com.zhangu.nutrition_calculator.AppAdapter.DietRecordAdapter;
import com.zhangu.nutrition_calculator.R;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DietRecordFragment extends Fragment implements DatePickerDialog.OnDateSetListener{
    public View view;
    public TextView totalIntakeEnergyTextView;
    public TextView canStillEatTextView;
    public TextView dateTv;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_diet_record, container, false);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(
                getContext(), R.style.AppTheme, this, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DATE));

        dateTv = (TextView)view.findViewById(R.id.dateTextView);
        dateTv.setText(getDateString(new Date()));
        dateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });


        totalIntakeEnergyTextView = (TextView) view.findViewById(R.id.totalIntakeEnergy);
        canStillEatTextView = (TextView) view.findViewById(R.id.canStillEatTextView);

        ((Button)view.findViewById(R.id.breakfastButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoActivity("早餐");
            }
        });

        ((Button)view.findViewById(R.id.lunchButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoActivity("午餐");
            }
        });

        ((Button)view.findViewById(R.id.dinnerButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoActivity("晚餐");
            }
        });

        ((Button)view.findViewById(R.id.moreMealButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoActivity("加餐");
            }
        });

        ((Button)view.findViewById(R.id.exerciseButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoActivity("运动");
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        showFoodRecord(new Date());
    }

    private void showFoodRecord(Date date){

        final AVQuery<AVObject> startDateQuery = new AVQuery<>("DietRecord");
        Date yesterday = yesterday(date);
        startDateQuery.whereGreaterThan("createdAt", yesterday);

        Date tomorrow = tomorrow(date);

        final AVQuery<AVObject> endDateQuery = new AVQuery<>("DietRecord");
        endDateQuery.whereLessThan("createdAt", tomorrow);

        AVQuery<AVObject> query = AVQuery.and(Arrays.asList(startDateQuery, endDateQuery));
        query.whereEqualTo("user", AVUser.getCurrentUser());
        query.include("user");
        query.include("food");

        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if(e == null){
                    ((TextView)view.findViewById(R.id.noFoodRecordTextView)).setVisibility(android.view.View.INVISIBLE);
                    ListAdapter foodListAdapter = new DietRecordAdapter(getActivity(),list);

                    ListView listView = (ListView)view.findViewById(R.id.foodRecordListView);
                    listView.setAdapter(foodListAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        }
                    });
                    Double intakeTitle = getTotalIntake(list);
                    Double canStillEat = 1705 - getTotalIntake(list);
                    totalIntakeEnergyTextView.setText(Double.toString(intakeTitle));
                    canStillEatTextView.setText(Double.toString(canStillEat));
                }else{
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        final Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, i);
        cal.set(Calendar.MONTH, i1);
        cal.set(Calendar.DATE, i2);
        showFoodRecord(cal.getTime());
        dateTv.setText(getDateString(cal.getTime()));
    }

    private Double getTotalIntake(List<AVObject> list){


        Double totalIntake = 0.0;
        for (AVObject foodRecord : list) {
            AVObject food = (AVObject)foodRecord.get("food");
            Double totalEnergy = foodRecord.getDouble("quantity") * food.getDouble("energy")/100;
            totalIntake = totalIntake + totalEnergy;
        }
        return totalIntake;
    }

    private void gotoActivity(String mealType){
        Intent intent = new Intent(getActivity(), AddFoodRecordActivity.class);
        intent.putExtra("mealType",mealType);
        startActivity(intent);
    }

    private Date yesterday(Date date) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 59);
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    private Date tomorrow(Date date) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DATE, 1);
        return cal.getTime();
    }

    private String getDateString(Date date){
        SimpleDateFormat simpleDate =  new SimpleDateFormat("yyyy-MM-dd");
        String strDt = simpleDate.format(date);
        return strDt;
    }

}
