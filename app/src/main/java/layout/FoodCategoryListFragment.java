package layout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.zhangu.nutrition_calculator.AppAdapter.FoodCategoryAdapter;
import com.zhangu.nutrition_calculator.FoodListActivity;
import com.zhangu.nutrition_calculator.R;

import java.util.List;

public class FoodCategoryListFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_food_list, container, false);

        final ListView foodCategoryListView = (ListView)view.findViewById(R.id.foodCategoryListView);

        AVQuery<AVObject> query = new AVQuery<>("FoodCategory");
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(final List<AVObject> list, AVException e) {

                ListAdapter foodCategoryAdapter = new FoodCategoryAdapter(getActivity(),list);

                foodCategoryListView.setAdapter(foodCategoryAdapter);

                foodCategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent(getActivity(), FoodListActivity.class);
                        AVObject category = (AVObject)list.get(i);
                        intent.putExtra("categoryAvObj",category);
                        startActivity(intent);
                    }
                });
            }
        });

        return view;
    }

}
